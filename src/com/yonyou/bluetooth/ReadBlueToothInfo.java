package com.yonyou.bluetooth;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;

import com.yonyou.uap.um.base.UMEventArgs;
import com.yonyou.zebraprint.util.UIHelper;
import com.zebra.sdk.printer.discovery.DiscoveredPrinter;
import com.zebra.sdk.printer.discovery.DiscoveryHandler;

public class ReadBlueToothInfo implements DiscoveryHandler{
	private static final int REQUEST_BLUETOOTH_PERMISSION=10;
	private static final UUID MY_UUID = UUID
		    .fromString("abcd1234-ab12-ab12-ab12-abcdef123456");//随便定义一个
	public static Activity activity = null;
	public static StringBuffer bluetoothAddress;
	public static boolean isComplement = false;
	public static BtBroadcastReceiver receiver = null;
	public static BluetoothSocket clientSocket = null;
	public static BluetoothDevice device = null;
	public static OutputStream os;//输出流
	public static BluetoothAdapter mBluetoothAdapter;
	public static String ADDR;
	public static UIHelper helper = null;
	public static ArrayList<HashMap<String,String>> printerList = null;
	public static StringBuffer buffer = null;
	public static void getBlueToothInfo(UMEventArgs args)throws IOException{
		activity= args.getUMActivity();
		bluetoothAddress = new StringBuffer();
		if(requestBluetoothPermission()){
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	  	  	mBluetoothAdapter.enable(); //开启
	  	  	//mBluetoothAdapter.disable(); //关闭
	  	  	//获取已经配对的蓝牙设备
	  	  	Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
	  	  	bluetoothAddress.append("{\"bluetooth\":[");
	  	  	if (pairedDevices.size() > 0) {	
	  	  		for (BluetoothDevice device : pairedDevices) {
	  	  			bluetoothAddress.append("{\"bt_name\":\""+device.getName()+"\"," + "\"bt_addr\":\""+device.getAddress() + "\"},");
	  	    	}
	  	  		bluetoothAddress.deleteCharAt(bluetoothAddress.length()-1);
	  	  	}
	  	  	bluetoothAddress.append("]}");
	  	  	/**
	  	  	 * 定义广播接收器
	  	  	 */
	  	  	if(receiver==null){
	  	  		receiver = new BtBroadcastReceiver();
    	  	
	  	  		// 设置广播信息过滤
	  	  		IntentFilter filter = new IntentFilter();
	  	  		filter.addAction(BluetoothDevice.ACTION_FOUND);//每搜索到一个设备就会发送一个该广播
	  	  		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);//当全部搜索完后发送该广播
	  	  		filter.setPriority(Integer.MAX_VALUE);//设置优先级
	  	  		// 注册蓝牙搜索广播接收者，接收并处理搜索结果
	  	  		activity.registerReceiver(receiver, filter);
	  	  	}
	    	 mBluetoothAdapter.startDiscovery();
	    	 return;
		}else{
			return;
		}
	}
	
	public static String getBlueToothStatus(UMEventArgs args)throws IOException{
		if(isComplement){
			isComplement = false;
			return "complement";
		}else{
			return "uncomplement";
		}
	}
	
	public static String getBlueToothList(UMEventArgs args)throws IOException{
		return bluetoothAddress.toString();
	}
	
	public static boolean getBlueToothConnect(UMEventArgs args)throws IOException{
		String addr = args.getString("param");
		try {
		        if(ADDR==null||!ADDR.equals(addr)){
		        	//获得远程设备
			        device = mBluetoothAdapter.getRemoteDevice(addr);
			        Method createBondMethod = BluetoothDevice.class.getMethod("createBond");     
	                createBondMethod.invoke(device);
		        }
		      if(device.getBondState()==BluetoothDevice.BOND_BONDED){
		    	  if (clientSocket == null) {
				      //创建客户端蓝牙Socket
				      clientSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
				      //开始连接蓝牙，如果没有配对则弹出对话框提示我们进行配对
				      clientSocket.connect();
				  }
		    	  return true;
		      }else{
		    	return false;  
		      }
		}catch (Exception e) {
			return false;
		}
	}
	
	public static void sendMsg(UMEventArgs args){
		try {
			//获得输出流（客户端指向服务端输出文本）
	        os = clientSocket.getOutputStream();
			os.write(args.getString("param").getBytes("utf-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}/*finally{
			try {
				os.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/
	}
	
	public static String getMsg(UMEventArgs args)throws IOException{
		
		return null;
	}
	
	public static String start(UMEventArgs args)throws IOException{
		activity = args.getUMActivity();
		helper = new UIHelper(activity);
		printerList = new ArrayList<HashMap<String,String>>();
//		connect.discoveryBTPirnter();
		return "success";
	}
	
	static class BtBroadcastReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
  	        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
  	          BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
  	          if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
  	        	  if(bluetoothAddress.charAt(bluetoothAddress.length()-3)=='['){
  	        		bluetoothAddress.insert(bluetoothAddress.length()-2,"{\"bt_name\":\""+device.getName()+"\"," + "\"bt_addr\":\""+device.getAddress() + "\"}");
  	        	  }else{
  	        		bluetoothAddress.insert(bluetoothAddress.length()-2,",{\"bt_name\":\""+device.getName()+"\"," + "\"bt_addr\":\""+device.getAddress() + "\"}");  
  	        	  }
  	          }
  	        } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
  	          //已搜素完成
  	        	isComplement = true;
  	        }
		}
    	
    }
	
	@SuppressLint("NewApi") private static boolean requestBluetoothPermission(){
  	  //判断系统版本
  	  if (Build.VERSION.SDK_INT >= 23) {
  	    //检测当前app是否拥有某个权限
  	    int checkCallPhonePermission = activity.checkSelfPermission( 
  	        Manifest.permission.ACCESS_COARSE_LOCATION);
  	    //判断这个权限是否已经授权过
  	    if(checkCallPhonePermission != PackageManager.PERMISSION_GRANTED){
  	      activity.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_BLUETOOTH_PERMISSION);
  	      return true;
  	    }else{
  	    	return false;
  	    }
  	  }
  	  return true;
  	}

	@Override
	public void discoveryError(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void discoveryFinished() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void foundPrinter(DiscoveredPrinter arg0) {
		// TODO Auto-generated method stub
		
	}
}	
